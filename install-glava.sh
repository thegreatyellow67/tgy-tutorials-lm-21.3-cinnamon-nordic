#!/bin/bash

clear

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
                                                                     
echo "  ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "  ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║     "
echo "  ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║     "
echo "  ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo ""
echo "  ===================================================================="
echo "  Script per installare il visualizzatore grafico Glava"
echo "  Scritto da TGY-TUTORIALS il 14/01/2024"
echo "  ===================================================================="

THEME="nordic"
THEME_FOLDER="lm-21.3-cinnamon-$THEME"
BASE_PATH="$HOME/Scaricati/"

echo ""
echo "  Sto installando alcuni pacchetti per la compilazione di glava, un pò di pazienza..."
echo ""

sudo apt install libgl1-mesa-dev libpulse0 libpulse-dev libxext6 libxext-dev libxrender-dev libxcomposite-dev liblua5.3-dev liblua5.3-0 lua-lgi lua-filesystem libobs0 libobs-dev meson build-essential gcc ccache -y &> /dev/null

sudo ldconfig
sudo ccache -c

echo ""
echo "  Compilo glava, un pò di pazienza..."
echo ""

cd ${BASE_PATH}/${THEME_FOLDER}/glava-src-gitlab
meson build --reconfigure --prefix /usr
ninja -C build
sudo ninja -C build install

