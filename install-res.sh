#!/bin/bash

clear

echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
                                                                     
echo "  ███╗   ██╗ ██████╗ ██████╗ ██████╗ ██╗ ██████╗"
echo "  ████╗  ██║██╔═══██╗██╔══██╗██╔══██╗██║██╔════╝"
echo "  ██╔██╗ ██║██║   ██║██████╔╝██║  ██║██║██║     "
echo "  ██║╚██╗██║██║   ██║██╔══██╗██║  ██║██║██║     "
echo "  ██║ ╚████║╚██████╔╝██║  ██║██████╔╝██║╚██████╗"
echo "  ╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚═════╝"
echo ""
echo "  ===================================================================="
echo "  Script per automatizzare la copia delle"
echo "  risorse per il tema Cinnamon Nordic"
echo "  Scritto da TGY-TUTORIALS il 14/01/2024"
echo "  ===================================================================="

THEME="nordic"
THEME_FOLDER="lm-21.3-cinnamon-$THEME"
BASE_PATH="$HOME/Scaricati/"

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-cinnamon-nordic.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git
  unzip lm21.3-cinnamon-nordic.zip &> /dev/null
  rm lm21.3-cinnamon-nordic.zip

  clear
  echo ""
  echo "  Sto scompattando le risorse, un pò di pazienza..."
  echo ""

  for file in *.zip;
  do
    unzip "$file" &> /dev/null
  done
  rm *.zip

  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo ""
  sudo apt install fonts-powerline conky-all jq playerctl vlc ffmpeg diodon dconf-editor tilix gpick gimp inkscape htop btop cava -y &> /dev/null

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo ""
  cp -r fonts-${THEME}/* ~/.fonts
  sudo cp -r fonts-${THEME}/Roboto* /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK Nordic..."
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone Tela, un pò di pazienza..."
  echo ""
  cp -r tela-icons/* ~/.icons
  sudo cp -r tela-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori Nordic..."
  echo ""
  cp -r ${THEME}-cursors/ ~/.icons
  sudo cp -r ${THEME}-cursors/ /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone per il Menù..."
  echo ""
  cp -r start-menu-icons/ ~/.icons
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  sleep 3

  clear
  echo ""
  echo "  Installazione delle azioni per Nemo..."
  echo ""
  cp -r nemo-actions/* ~/.local/share/nemo/actions
  sleep 3

  clear
  echo ""
  echo "  Installazione dei temi di oh-my-posh..."
  echo ""
  cp -r oh-my-posh-themes/.oh-my-posh-themes/ ~/
  sleep 3

  clear
  echo ""
  echo "  Installazione dello script di Conky..."
  echo ""
  cp -r conky-config/conky/* ~/.config/conky
  cp conky-config/autostart/* ~/.config/autostart
  cp conky-config/autostart/* ~/.local/share/applications
  sleep 3

  clear
  echo ""
  echo "  Installazione tema, applicazioni e configurazione di Plank..."
  echo ""
  cp -r plank-config/.config/* ~/.config
  cp -r plank-config/.local/share/plank/ ~/.local/share
  cp -r plank-config/.local/share/applications/* ~/.local/share/applications
  dconf load /net/launchpad/plank/ < plank-config/plank-settings.conf
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Cava..."
  echo ""
  cp -r cava-config/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Glava..."
  echo ""
  cp -r glava-config/.config/glava/ ~/.config
  cp -r glava-config/.config/autostart/* ~/.config/autostart
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione di macchina e relativa configurazione..."
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  cp -r macchina/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di neofetch..."
  echo ""
  cp -r neofetch/ ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy per Grub..."
  echo ""
  sudo cp -r sugar-candy/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema per Ulauncher..."
  echo ""
  cp -r ulauncher/ulauncher-theme/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Risorse installate con successo!"
  sleep 3
  echo ""

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
